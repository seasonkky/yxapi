package com.yx.apitest;

import android.content.Context;
import android.os.Bundle;
import android.os.yx.YxDeviceManager;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Arrays;
import java.util.Calendar;

public class PowerOnOffActivity extends AppCompatActivity implements View.OnClickListener{
    public String TAG = "yxapitest";
    private Toast toast;
    private YxDeviceManager yxDeviceManager;

    private EditText powerOffHour;
    private EditText powerOffMinute;
    private EditText poweronHour;
    private EditText poweronMinute;
    private EditText poweronoff_onYear;
    private EditText poweronoff_onMonth;
    private EditText poweronoff_onDate;
    private EditText poweronoff_onHour;
    private EditText poweronoff_onMinute;
    private EditText poweronoff_offYear;
    private EditText poweronoff_offMonth;
    private EditText poweronoff_offDate;
    private EditText poweronoff_offHour;
    private EditText poweronoff_offMinute;
    private CheckBox mon;
    private CheckBox tue;
    private CheckBox wen;
    private CheckBox thr;
    private CheckBox fri;
    private CheckBox sat;
    private CheckBox sun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power_on_off);

        init();
    }
    public void init(){
        yxDeviceManager = YxDeviceManager.getInstance(this);

        findViewById(R.id.getPowerOnMode).setOnClickListener(this);
        findViewById(R.id.getPowerOnTime).setOnClickListener(this);
        findViewById(R.id.getPowerOffTime).setOnClickListener(this);
        findViewById(R.id.getLastestPowerOnTime).setOnClickListener(this);
        findViewById(R.id.getLastestPowerOffTime).setOnClickListener(this);
        findViewById(R.id.clearPowerOnOffTime).setOnClickListener(this);
        findViewById(R.id.isSetPowerOnTime).setOnClickListener(this);
        findViewById(R.id.setVendingLoseTimer).setOnClickListener(this);

        findViewById(R.id.setPowerOnOffWithWeekly).setOnClickListener(this);
        findViewById(R.id.setPowerOnOff).setOnClickListener(this);

        poweronHour = findViewById(R.id.poweron_hour);
        poweronMinute = findViewById(R.id.poweron_minute);
        powerOffHour = findViewById(R.id.poweroff_hour);
        powerOffMinute = findViewById(R.id.poweroff_minute);

        poweronoff_onYear = findViewById(R.id.poweronoff_on_year);
        poweronoff_onMonth = findViewById(R.id.poweronoff_on_month);
        poweronoff_onDate = findViewById(R.id.poweronoff_on_date);
        poweronoff_onHour = findViewById(R.id.poweronoff_on_hour);
        poweronoff_onMinute = findViewById(R.id.poweronoff_on_minute);

        poweronoff_offYear = findViewById(R.id.poweronoff_off_year);
        poweronoff_offMonth = findViewById(R.id.poweronoff_off_month);
        poweronoff_offDate = findViewById(R.id.poweronoff_off_date);
        poweronoff_offHour = findViewById(R.id.poweronoff_off_hour);
        poweronoff_offMinute = findViewById(R.id.poweronoff_off_minute);

        mon = findViewById(R.id.mon);
        tue = findViewById(R.id.tue);
        wen = findViewById(R.id.wen);
        thr = findViewById(R.id.thr);
        fri = findViewById(R.id.fri);
        sat = findViewById(R.id.sat);
        sun = findViewById(R.id.sun);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.getPowerOnMode:
                showToast(this,yxDeviceManager.getPowerOnMode());
                break;
            case R.id.getPowerOnTime:
                showToast(this,yxDeviceManager.getPowerOnTime());
                break;
            case R.id.getPowerOffTime:
                showToast(this,yxDeviceManager.getPowerOffTime());
                break;
            case R.id.getLastestPowerOnTime:
                showToast(this,yxDeviceManager.getLastestPowerOnTime());
                break;
            case R.id.getLastestPowerOffTime:
                showToast(this,yxDeviceManager.getLastestPowerOffTime());
                break;
            case R.id.clearPowerOnOffTime:
                yxDeviceManager.clearPowerOnOffTime();
                break;
            case R.id.isSetPowerOnTime:
                showToast(this," " + yxDeviceManager.isSetPowerOnTime());
                break;
            case R.id.setVendingLoseTimer:
                showToast(this," " + yxDeviceManager.setVendingLoseTimer(0));
                break;
            case R.id.setPowerOnOffWithWeekly:
                int onMinute1 = Integer.parseInt(poweronMinute.getText().toString().trim());
                int onHour1 = Integer.parseInt(poweronHour.getText().toString().trim());
                int offMinute1 = Integer.parseInt(powerOffMinute.getText().toString().trim());
                int offHour1 = Integer.parseInt(powerOffHour.getText().toString().trim());

                int[] timeoffArray1 = new int[2];
                int[] timeonArray1 = new int[2];
                timeonArray1[0] = onHour1;
                timeonArray1[1] = onMinute1;
                timeoffArray1[0] = offHour1;
                timeoffArray1[1] = offMinute1;

                int[] wkdays = new int[7];
                wkdays[0] = getWeekState(mon.isChecked());
                wkdays[1] = getWeekState(tue.isChecked());
                wkdays[2] = getWeekState(wen.isChecked());
                wkdays[3] = getWeekState(thr.isChecked());
                wkdays[4] = getWeekState(fri.isChecked());
                wkdays[5] = getWeekState(sat.isChecked());
                wkdays[6] = getWeekState(sun.isChecked());
                yxDeviceManager.setPowerOnOffWithWeekly(timeonArray1,timeoffArray1,wkdays);
                showToast(this,"poweron:" + Arrays.toString(timeonArray1) + "poweroff:" + Arrays.toString(timeoffArray1) + "weekday:" + Arrays.toString(wkdays));
                break;
            case R.id.setPowerOnOff:
                int offYear = Integer.parseInt(poweronoff_offYear.getText().toString().trim());
                int offMonth = Integer.parseInt(poweronoff_offMonth.getText().toString().trim());
                int offDate = Integer.parseInt(poweronoff_offDate.getText().toString().trim());
                int offHour = Integer.parseInt(poweronoff_offHour.getText().toString().trim());
                int offMinute = Integer.parseInt(poweronoff_offMinute.getText().toString().trim());

                int onYear = Integer.parseInt(poweronoff_onYear.getText().toString().trim());
                int onMonth = Integer.parseInt(poweronoff_onMonth.getText().toString().trim());
                int onDate = Integer.parseInt(poweronoff_onDate.getText().toString().trim());
                int onHour = Integer.parseInt(poweronoff_onHour.getText().toString().trim());
                int onMinute = Integer.parseInt(poweronoff_onMinute.getText().toString().trim());
                int[] timeoffArray = new int[5];
                timeoffArray[0] = offYear;
                timeoffArray[1] = offMonth;
                timeoffArray[2] = offDate;
                timeoffArray[3] = offHour;
                timeoffArray[4] = offMinute;

                int[] timeonArray = new int[5];
                timeonArray[0] = onYear;
                timeonArray[1] = onMonth;
                timeonArray[2] = onDate;
                timeonArray[3] = onHour;
                timeonArray[4] = onMinute;
                yxDeviceManager.setPowerOnOff(timeonArray,timeoffArray);
                showToast(this,"poweron:" + Arrays.toString(timeonArray) + "poweroff:" + Arrays.toString(timeoffArray));
                break;
            default:
                break;
        }
    }
    private int getWeekState(boolean flag) {
        int i;
        if (flag)
            i = 1;
        else
            i = 0;
        return i;
    }
    public void showToast(Context context, String message){
        Log.d(TAG, message);
        if(toast!=null){
            toast.cancel();
            toast = null;
        }
        if(toast==null){
            toast = Toast.makeText(this, message,Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
