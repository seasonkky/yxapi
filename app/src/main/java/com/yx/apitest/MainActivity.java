package com.yx.apitest;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.hardware.Camera;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.yx.IYxInstallListener;
import android.os.yx.YxDeviceManager;
import android.os.Bundle;
import android.os.yx.YxWiegand;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String FILENAME = "blackapplist";
    public String TAG = "yxapitest";
    private YxDeviceManager yxDeviceManager;
    private YxWiegand yxWiegand;
    private Toast toast;
    private String IPADDR = "192.168.0.125";
    private String MASK = "255.255.255.0";
    private String GATEWAY = "192.168.0.1";
    private String DNS1 = "192.168.0.1";
    private String DNS2 = "0.0.0.0";
    private String externalStoragePath;
    private int angle;
    Intent intent;
    private String mIfaceName = "eth0" ;//"eth1";
    private byte[] wiegand;

    private IYxInstallListener aListener = new IYxInstallListener.Stub() {
        @Override
        public void onNewValue(int i) throws RemoteException {
            Log.d(TAG, "APK INSTALL STATUS : " + i);
            if(i==0){
                Log.d(TAG, "APK INSTALL SUCCESS!" );
                showToast(MainActivity.this, "APK INSTALL SUCCESS!");
            }else {
                Log.d(TAG, "APK INSTALL FAIL");
                showToast(MainActivity.this, "APK INSTALL FAIL");
            }
        }
    };
    YxWiegand.YxWiegandCallBack yxWiegandCallBack = new YxWiegand.YxWiegandCallBack() {
        @Override
        public void onNewValue(byte[] bytes) {
            Log.d(TAG,"callback data");
        }
    };
    @Override
    protected void onResume(){
        super.onResume();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }
    public void init(){
        yxDeviceManager = YxDeviceManager.getInstance(this);
        yxWiegand = YxWiegand.getInstance(this);
        wiegand = new byte[]{0};//data need customer defend
        //externalStoragePath = Environment.getExternalStorageDirectory().getPath();
        Log.d(TAG, "externalStoragePath = " + externalStoragePath);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        filter.addDataScheme("file");
        registerReceiver(mountReceiver, filter);

        //system information
        findViewById(R.id.getAndroidModle).setOnClickListener(this);
        findViewById(R.id.getAndroidVersion).setOnClickListener(this);
        findViewById(R.id.getRunningMemory).setOnClickListener(this);
        findViewById(R.id.getInternalStorageMemory).setOnClickListener(this);
        findViewById(R.id.getKernelVersion).setOnClickListener(this);
        findViewById(R.id.getAndroidDisplay).setOnClickListener(this);
        findViewById(R.id.getCPUType).setOnClickListener(this);
        findViewById(R.id.getFirmwareDate).setOnClickListener(this);
        findViewById(R.id.getApiVersion).setOnClickListener(this);
        findViewById(R.id.getTelephonyImei).setOnClickListener(this);
        findViewById(R.id.getSimSerialNumber).setOnClickListener(this);
        findViewById(R.id.getSerialno).setOnClickListener(this);
        findViewById(R.id.setDeviceSerialno).setOnClickListener(this);
        findViewById(R.id.setDeviceCustom).setOnClickListener(this);
        findViewById(R.id.getDeviceCustom).setOnClickListener(this);
        //poweroff&reboot
        findViewById(R.id.shutDownNow).setOnClickListener(this);
        findViewById(R.id.rebootNow).setOnClickListener(this);
        //display
        findViewById(R.id.setNavBarShow).setOnClickListener(this);
        findViewById(R.id.setNavBarHide).setOnClickListener(this);
        findViewById(R.id.setStaBarShow).setOnClickListener(this);
        findViewById(R.id.setStaBarHide).setOnClickListener(this);
        findViewById(R.id.setScreenRotation).setOnClickListener(this);
        findViewById(R.id.getScreenRotation).setOnClickListener(this);
        findViewById(R.id.setLedStatus).setOnClickListener(this);
        findViewById(R.id.getLedStatus).setOnClickListener(this);
        findViewById(R.id.setBrightness).setOnClickListener(this);
        findViewById(R.id.getBrightness).setOnClickListener(this);
        findViewById(R.id.turnOffBacklight).setOnClickListener(this);
        findViewById(R.id.turnOnBacklight).setOnClickListener(this);
        findViewById(R.id.isBacklightOn).setOnClickListener(this);
        findViewById(R.id.setBacklightBrightness).setOnClickListener(this);
        findViewById(R.id.getSystemBrightness).setOnClickListener(this);
        findViewById(R.id.getNavBarHideState).setOnClickListener(this);
        findViewById(R.id.isSlideShowNavBarOpen).setOnClickListener(this);
        findViewById(R.id.setSlideShowNavBar).setOnClickListener(this);
        findViewById(R.id.notsetSlideShowNavBar).setOnClickListener(this);
        findViewById(R.id.turnOffBacklightExtend).setOnClickListener(this);
        findViewById(R.id.turnOnBacklightExtend).setOnClickListener(this);
        findViewById(R.id.isBacklightOnExtend).setOnClickListener(this);
        findViewById(R.id.getSystemBrightnessExtend).setOnClickListener(this);
        findViewById(R.id.turnOnHDMI).setOnClickListener(this);
        findViewById(R.id.turnOffHDMI).setOnClickListener(this);
        findViewById(R.id.setSlideShowNotificationBar).setOnClickListener(this);
        findViewById(R.id.setSlideShowNotificationBarDisable).setOnClickListener(this);
        findViewById(R.id.setPwmDuty).setOnClickListener(this);
        findViewById(R.id.getPwmDuty).setOnClickListener(this);
        //install&upgrade
        findViewById(R.id.silentInstallApk).setOnClickListener(this);
        findViewById(R.id.silentInstallApkautostart).setOnClickListener(this);
        findViewById(R.id.silentInstallApkcallback).setOnClickListener(this);
        findViewById(R.id.unInstallApk).setOnClickListener(this);
        findViewById(R.id.upgradeSystem).setOnClickListener(this);
        findViewById(R.id.setUpdateSystemWithDialog).setOnClickListener(this);
        findViewById(R.id.notsetUpdateSystemWithDialog).setOnClickListener(this);
        findViewById(R.id.setUpdateSystemDelete).setOnClickListener(this);
        findViewById(R.id.notsetUpdateSystemDelete).setOnClickListener(this);
        //internet
        findViewById(R.id.setDeviceMacaddress).setOnClickListener(this);
        findViewById(R.id.getDeviceMacaddress).setOnClickListener(this);
        findViewById(R.id.getEthMode).setOnClickListener(this);
        findViewById(R.id.getEthStatus).setOnClickListener(this);
        findViewById(R.id.getNetMask).setOnClickListener(this);
        findViewById(R.id.getGateway).setOnClickListener(this);
        findViewById(R.id.getEthDns1).setOnClickListener(this);
        findViewById(R.id.getEthDns2).setOnClickListener(this);
        findViewById(R.id.getIpAddress).setOnClickListener(this);
        findViewById(R.id.ethEnabled).setOnClickListener(this);
        findViewById(R.id.ethDisenabled).setOnClickListener(this);
        findViewById(R.id.setStaticEthIPAddress).setOnClickListener(this);
        findViewById(R.id.setEthernetDhcp).setOnClickListener(this);
        findViewById(R.id.setWifiStaticIP).setOnClickListener(this);
        //time poweronoff
        findViewById(R.id.time_poweronoff).setOnClickListener(this);
        //gpio test
        findViewById(R.id.gpio_test).setOnClickListener(this);
        //other
        findViewById(R.id.replaceBootanimation).setOnClickListener(this);
        findViewById(R.id.setSystemTime).setOnClickListener(this);
        findViewById(R.id.enableWatchdog).setOnClickListener(this);
        findViewById(R.id.disableWatchdog).setOnClickListener(this);
        findViewById(R.id.feedWatchdog).setOnClickListener(this);
        findViewById(R.id.getOtgMode).setOnClickListener(this);
        findViewById(R.id.setOtgMode).setOnClickListener(this);
        findViewById(R.id.notsetOtgMode).setOnClickListener(this);
        findViewById(R.id.setRelayStatus).setOnClickListener(this);
        findViewById(R.id.setIrLight).setOnClickListener(this);
        findViewById(R.id.setScreenKeyGuard).setOnClickListener(this);
        findViewById(R.id.getDcStatus).setOnClickListener(this);
        findViewById(R.id.changeMobileCarrier).setOnClickListener(this);
        findViewById(R.id.setMcuReboot).setOnClickListener(this);
        findViewById(R.id.setBugReport).setOnClickListener(this);
        findViewById(R.id.notsetBugReport).setOnClickListener(this);
        findViewById(R.id.getBugReport).setOnClickListener(this);
        findViewById(R.id.setOemFunc).setOnClickListener(this);
        findViewById(R.id.setSensorReport).setOnClickListener(this);
        findViewById(R.id.getSensorReport).setOnClickListener(this);
        findViewById(R.id.setLedBlink).setOnClickListener(this);
        findViewById(R.id.setModem).setOnClickListener(this);
        findViewById(R.id.getRootStatus).setOnClickListener(this);
        findViewById(R.id.selfStart).setOnClickListener(this);
        findViewById(R.id.getDefaultInputMethod).setOnClickListener(this);
        findViewById(R.id.isSetDefaultInputMethodSuccess).setOnClickListener(this);
        findViewById(R.id.setDefaultLauncher).setOnClickListener(this);
        findViewById(R.id.isAutoSyncTime).setOnClickListener(this);
        findViewById(R.id.switchAutoTime).setOnClickListener(this);
        findViewById(R.id.switchAutoTimeoff).setOnClickListener(this);
        findViewById(R.id.setLanguage).setOnClickListener(this);
        findViewById(R.id.getDpi).setOnClickListener(this);
        findViewById(R.id.getDisplayWidth).setOnClickListener(this);
        findViewById(R.id.getDisplayHeight).setOnClickListener(this);
        findViewById(R.id.setDormantInterval).setOnClickListener(this);
        findViewById(R.id.getCPUTemperature).setOnClickListener(this);
        findViewById(R.id.setNetworkAdb).setOnClickListener(this);
        findViewById(R.id.notsetNetworkAdb).setOnClickListener(this);
        findViewById(R.id.daemon).setOnClickListener(this);
        findViewById(R.id.setAppInstallWhitelist).setOnClickListener(this);
        findViewById(R.id.setAppInstallBlacklist).setOnClickListener(this);
        findViewById(R.id.removeFileData).setOnClickListener(this);
        findViewById(R.id.removeFileDatablack).setOnClickListener(this);
        findViewById(R.id.takeScreenshot).setOnClickListener(this);
        findViewById(R.id.updateWifiConfigStore).setOnClickListener(this);
        findViewById(R.id.wiegandwrite).setOnClickListener(this);
        findViewById(R.id.wiegandstartreading).setOnClickListener(this);
        findViewById(R.id.wiegandstopreading).setOnClickListener(this);
        findViewById(R.id.wiegandrealese).setOnClickListener(this);
        findViewById(R.id.cam_osd_enable).setOnClickListener(this);
        findViewById(R.id.cam_osd_disable).setOnClickListener(this);
        findViewById(R.id.cam_osd_mirror).setOnClickListener(this);
        findViewById(R.id.cam_osd_pos).setOnClickListener(this);
        findViewById(R.id.getTempConf).setOnClickListener(this);
        findViewById(R.id.setTempConf).setOnClickListener(this);
        findViewById(R.id.getTempOffset).setOnClickListener(this);
        findViewById(R.id.setTempOffset).setOnClickListener(this);
        findViewById(R.id.getTempStatus).setOnClickListener(this);
        findViewById(R.id.getCompressStatus).setOnClickListener(this);
        findViewById(R.id.setCompressStatus).setOnClickListener(this);
        findViewById(R.id.getEthernetTetheringAddress).setOnClickListener(this);
        findViewById(R.id.setEthernetTetheringAddress).setOnClickListener(this);
        findViewById(R.id.startEthernetTethering).setOnClickListener(this);
        findViewById(R.id.stopEthernetTethering).setOnClickListener(this);
    }
    public  void ysInstall(String apkpath,Boolean isStart){
        Intent intent1 = new Intent("com.ys.installapk");
        intent1.putExtra("uri",apkpath);
        intent1.putExtra("start",isStart);
        this.sendBroadcast(intent1);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onClick(View v) {
        switch (v.getId()) {
            //system information
            case R.id.getAndroidModle:
                //Log.d(TAG, yxDeviceManager.getAndroidModle());
                //Toast.makeText(MainActivity.this, yxDeviceManager.getAndroidModle(),Toast.LENGTH_SHORT).show();
                showToast(this,yxDeviceManager.getAndroidModle());
                break;
            case R.id.getAndroidVersion:
                showToast(this,yxDeviceManager.getAndroidVersion());
                break;
            case R.id.getRunningMemory:
                showToast(this, yxDeviceManager.getRunningMemory());
                break;
            case R.id.getInternalStorageMemory:
                showToast(this, yxDeviceManager.getInternalStorageMemory());
                break;
            case R.id.getKernelVersion:
                showToast(this, yxDeviceManager.getKernelVersion());
                break;
            case R.id.getAndroidDisplay:
                showToast(this, yxDeviceManager.getAndroidDisplay());
                break;
            case R.id.getCPUType:
                showToast(this, yxDeviceManager.getCPUType());
                break;
            case R.id.getFirmwareDate:
                showToast(this, yxDeviceManager.getFirmwareDate());
                break;
            case R.id.getApiVersion:
                showToast(this, yxDeviceManager.getApiVersion());
                break;
            case R.id.getTelephonyImei:
                showToast(this, yxDeviceManager.getTelephonyImei());
                break;
            case R.id.getSimSerialNumber:
                showToast(this, yxDeviceManager.getSimSerialNumber());
                break;
            case R.id.getSerialno:
                showToast(this, yxDeviceManager.getSerialno());
                break;
            case R.id.setDeviceSerialno:
                //Log.d(TAG,getTime()+getRandomNum(6));
                showToast(this," "+yxDeviceManager.setDeviceSerialno(getTime()+getRandomNum(6)));
                break;
            case R.id.setDeviceCustom:
                showToast(this," "+yxDeviceManager.setDeviceCustom(0x10,"88888888"));
                break;
            case R.id.getDeviceCustom:
                showToast(this, yxDeviceManager.getDeviceCustom(0x10));
                break;
            //poweroff&reboot
            case R.id.shutDownNow:
                yxDeviceManager.shutDownNow();
                break;
            case R.id.rebootNow:
                yxDeviceManager.rebootNow();
                break;
            //display
            case R.id.setNavBarShow:
                yxDeviceManager.setNavBarNew(true);
                break;
            case R.id.setNavBarHide:
                yxDeviceManager.setNavBarNew(false);
                break;
            case R.id.setStaBarShow:
                yxDeviceManager.setStaBarNew(true);
                break;
            case R.id.setStaBarHide:
                yxDeviceManager.setStaBarNew(false);
                break;
            case R.id.setScreenRotation:
                angle = yxDeviceManager.getScreenRotation()+90;
                if(angle==360){
                    angle=0;
                }
                yxDeviceManager.setScreenRotation(angle);
                showToast(this, getString(R.string.setScreenRotation) + angle + getString(R.string.rebootNow));
                break;
            case R.id.getScreenRotation:
                showToast(this, " " + yxDeviceManager.getScreenRotation());
                break;
            case R.id.setLedStatus:
                yxDeviceManager.setLedStatus(90,true);
                break;
            case R.id.getLedStatus:
                showToast(this, " " + yxDeviceManager.getLedStatus(90));
                break;
            case R.id.setBrightness:
                yxDeviceManager.setBrightness(90);
                break;
            case R.id.getBrightness:
                showToast(this, " " + yxDeviceManager.getBrightness());
                break;
            case R.id.turnOffBacklight:
                yxDeviceManager.turnOffBacklight();
                break;
            case R.id.turnOnBacklight:
                yxDeviceManager.turnOnBacklight();
                break;
            case R.id.turnOffBacklightExtend:
                yxDeviceManager.turnOffBacklightExtend();
                break;
            case R.id.turnOnBacklightExtend:
                yxDeviceManager.turnOnBacklightExtend();
                break;
            case R.id.isBacklightOn:
                showToast(this, " " + yxDeviceManager.isBacklightOn());
                break;
            case R.id.setBacklightBrightness:
                yxDeviceManager.setBacklightBrightness(90);
                break;
            case R.id.getSystemBrightness:
                showToast(this, " " +yxDeviceManager.getSystemBrightness());
                break;
            case R.id.getNavBarHideState:
                showToast(this, " " +yxDeviceManager.getNavBarHideState());
                break;
            case R.id.isSlideShowNavBarOpen:
                showToast(this, " " +yxDeviceManager.isSlideShowNavBarOpen());
                break;
            case R.id.setSlideShowNavBar:
                yxDeviceManager.setSlideShowNavBar(true);
                break;
            case R.id.notsetSlideShowNavBar:
                yxDeviceManager.setSlideShowNavBar(false);
                break;
            case R.id.turnOnHDMI:
                yxDeviceManager.turnOnHDMI();
                break;
            case R.id.turnOffHDMI:
                yxDeviceManager.turnOffHDMI();
                break;
            case R.id.setSlideShowNotificationBar:
                yxDeviceManager.setSlideShowNotificationBar(true);
                break;
            case R.id.setSlideShowNotificationBarDisable:
                yxDeviceManager.setSlideShowNotificationBar(false);
                break;
            case R.id.setPwmDuty:
                yxDeviceManager.setPwmDuty(2500);
                break;
            case R.id.getPwmDuty:
                showToast(this, " "+yxDeviceManager.getPwmDuty());
                break;

            //install&upgrade
            case R.id.silentInstallApk:
                if(externalStoragePath!=null) {
                    yxDeviceManager.silentInstallApk(externalStoragePath + "/com.bjw.ComAssistant_2.apk");
                }else{
                    showToast(this,"请重新插拔U盘");
                }
                break;
            case R.id.silentInstallApkautostart:
                if(externalStoragePath!=null) {
                    yxDeviceManager.silentInstallApk(externalStoragePath + "/com.bjw.ComAssistant_2.apk",true);
                }else{
                    showToast(this,"请重新插拔U盘");
                    //yxDeviceManager.silentInstallApk("/sdcard/com.bjw.ComAssistant_2.apk",true);
                    //ysInstall("/sdcard/com.bjw.ComAssistant_2.apk",true);
                }
                break;
            case R.id.silentInstallApkcallback:
                if(externalStoragePath!=null) {
                    yxDeviceManager.silentInstallApk(aListener,externalStoragePath + "/com.bjw.ComAssistant_2.apk",true);
                }else{
                    showToast(this,"请重新插拔U盘callback");
                }
                break;
            case R.id.unInstallApk:
                yxDeviceManager.unInstallApk("com.bjw.ComAssistant");
                //yxDeviceManager.unInstallApk("com.yx.apitest");
                //yxDeviceManager.unInstallApk("com.tcn.vending");
                break;
            case R.id.upgradeSystem:
                if (Build.VERSION.SDK_INT < 25) {
                    if (new File(externalStoragePath + "/Download/update.img").exists()) {
                        yxDeviceManager.upgradeSystem(externalStoragePath + "/Download/update.img");
                    }else{
                        showToast(this,"请在内置存储Download下放置img升级包");
                    }
                } else {
//                    if (new File("/storage/emulated/0" + "/Download/update.zip").exists()){
//                        yxDeviceManager.upgradeSystem("/storage/emulated/0" + "/Download/update.zip");
//                    }else{
//                        showToast(this,"请在内置存储Download下放置zip升级包");
//                    }
                    if(externalStoragePath!=null) {
                        if (new File(externalStoragePath + "/Download/update.zip").exists()) {
                            yxDeviceManager.upgradeSystem(externalStoragePath + "/Download/update.zip");
                        }else{
                            showToast(this,""+externalStoragePath+"/Download下放置zip升级包");
                        }
                    }else{
                        showToast(this,"请重新插拔U盘");
                    }
                }
                Log.d(TAG, "externalStoragePath = " + externalStoragePath);
                break;
            case R.id.setUpdateSystemWithDialog:
                yxDeviceManager.setUpdateSystemWithDialog(true);
                break;
            case R.id.notsetUpdateSystemWithDialog:
                yxDeviceManager.setUpdateSystemWithDialog(false);
                break;
            case R.id.setUpdateSystemDelete:
                yxDeviceManager.setUpdateSystemDelete(true);
                break;
            case R.id.notsetUpdateSystemDelete:
                yxDeviceManager.setUpdateSystemDelete(false);
                break;
            //internet
            case R.id.setDeviceMacaddress:
                showToast(this, " " + yxDeviceManager.setDeviceMacaddress("aa:bb:cc:dd:ee:11;22:33:44:55:66:77"));
                break;
            case R.id.getDeviceMacaddress:
                showToast(this, yxDeviceManager.getDeviceMacaddress());
                break;
            case R.id.getEthMode:
                //showToast(this, " " + yxDeviceManager.getEthMode(mIfaceName));
                showToast(this, " " + yxDeviceManager.getEthMode());
                break;
            case R.id.getEthStatus:
                //showToast(this, " " + yxDeviceManager.getEthStatus(mIfaceName));
                showToast(this, " " + yxDeviceManager.getEthStatus());
                break;
            case R.id.getNetMask:
                //showToast(this, " " + yxDeviceManager.getNetMask(mIfaceName));
                showToast(this, " " + yxDeviceManager.getNetMask());
                break;
            case R.id.getGateway:
                //showToast(this, " " + yxDeviceManager.getGateway(mIfaceName));
                showToast(this, " " + yxDeviceManager.getGateway());
                break;
            case R.id.getEthDns1:
                //showToast(this, " " + yxDeviceManager.getEthDns1(mIfaceName));
                showToast(this, " " + yxDeviceManager.getEthDns1());
                break;
            case R.id.getEthDns2:
                //showToast(this, " " + yxDeviceManager.getEthDns2(mIfaceName));
                showToast(this, " " + yxDeviceManager.getEthDns2());
                break;
            case R.id.getIpAddress:
                //showToast(this, " " + yxDeviceManager.getIpAddress(mIfaceName));
                showToast(this, " " + yxDeviceManager.getIpAddress());
                break;
            case R.id.ethEnabled:
                //yxDeviceManager.ethEnabled(mIfaceName,true);
                yxDeviceManager.ethEnabled(true);
                break;
            case R.id.ethDisenabled:
                //yxDeviceManager.ethEnabled(mIfaceName,false);
                yxDeviceManager.ethEnabled(false);
                break;
            case R.id.setStaticEthIPAddress:
                //yxDeviceManager.setStaticEthIPAddress(mIfaceName,IPADDR,GATEWAY,MASK,DNS1,DNS2);
                yxDeviceManager.setStaticEthIPAddress(IPADDR,GATEWAY,MASK,DNS1,DNS2);
                showToast(this, " "+IPADDR+" "+GATEWAY+" "+MASK+" "+DNS1+" "+DNS2);
                break;
            case R.id.setEthernetDhcp://
                //yxDeviceManager.ethEnabled(mIfaceName,false);
                yxDeviceManager.setEthernetDhcp();
                break;
            case R.id.setWifiStaticIP://
                //yxDeviceManager.setWifiStaticIP("192.168.3.235","192.168.3.1",MASK,"192.168.3.1",DNS2);
                yxDeviceManager.setWifiStaticIP(IPADDR,GATEWAY,MASK,DNS1,DNS2);
                break;
            //time poweronoff
            case R.id.time_poweronoff:
                Intent timeintent = new Intent(MainActivity.this, PowerOnOffActivity.class);
                startActivity(timeintent);
                break;
            //gpio test
            case R.id.gpio_test:
                Intent gpiointent = new Intent(MainActivity.this, GPIOTestActivity.class);
                startActivity(gpiointent);
                break;
            //other
            case R.id.replaceBootanimation:
                if(externalStoragePath!=null) {
                    yxDeviceManager.replaceBootanimation(externalStoragePath + "/bootanimation.zip");
                }else{
                    showToast(this,"请重新插拔U盘");
                }
                break;
            case R.id.setSystemTime:
                yxDeviceManager.setSystemTime(100000000);
                break;
            case R.id.enableWatchdog:
                yxDeviceManager.enableWatchdog();
                break;
            case R.id.disableWatchdog:
                yxDeviceManager.disableWatchdog();
                break;
            case R.id.feedWatchdog:
                yxDeviceManager.feedWatchdog();
                break;
            case R.id.getOtgMode:
                showToast(this, " " + yxDeviceManager.getOtgMode());
                break;
            case R.id.setOtgMode:
                yxDeviceManager.setOtgMode(true);
                break;
            case R.id.notsetOtgMode:
                yxDeviceManager.setOtgMode(false);
                break;
            case R.id.setRelayStatus:
                yxDeviceManager.setRelayStatus(true);
                break;
            case R.id.setIrLight:
                yxDeviceManager.setIrLight(true);
                break;
            case R.id.setScreenKeyGuard:
                yxDeviceManager.setScreenKeyGuard(false);
                mHandler.sendEmptyMessageDelayed(0,2000);
                break;
            case R.id.getDcStatus:
                showToast(this, " " + yxDeviceManager.getDcStatus());
                break;
            case R.id.changeMobileCarrier:
                yxDeviceManager.changeMobileCarrier(3);
                break;
            case R.id.setMcuReboot:
                yxDeviceManager.setMcuReboot();
                break;
            case R.id.setBugReport:
                yxDeviceManager.setBugReport(true);
                break;
            case R.id.notsetBugReport:
                yxDeviceManager.setBugReport(false);
                break;
            case R.id.getBugReport:
                showToast(this, " " + yxDeviceManager.getBugReport());
                break;
            case R.id.setOemFunc:
                yxDeviceManager.setOemFunc("");
                break;
            case R.id.setSensorReport:
                yxDeviceManager.setSensorReport(1);
                break;
            case R.id.getSensorReport:
                showToast(this, " " + yxDeviceManager.getSensorReport());
                break;
            case R.id.setLedBlink:
                yxDeviceManager.setLedBlink(1,1000);
                break;
            case R.id.setModem:
                yxDeviceManager.setModem(0);
                break;
            case R.id.getRootStatus:
                showToast(this, " " + yxDeviceManager.getRootStatus());
                break;
            case R.id.selfStart:
                yxDeviceManager.selfStart("com.bjw.ComAssistant");
                break;
            case R.id.getDefaultInputMethod:
                showToast(this, " "+yxDeviceManager.getDefaultInputMethod());
                break;
            case R.id.isSetDefaultInputMethodSuccess://Permission system
                showToast(this, " "+yxDeviceManager.isSetDefaultInputMethodSuccess(""));
                break;
            case R.id.setDefaultLauncher:
                yxDeviceManager.setDefaultLauncher("com.android.launcher3/com.android.launcher3.uioverrides.QuickstepLauncher");
                break;
            case R.id.isAutoSyncTime:
                showToast(this, " "+yxDeviceManager.isAutoSyncTime());
                break;
            case R.id.getDpi:
                showToast(this, " "+yxDeviceManager.getDpi());
                break;
            case R.id.getDisplayWidth:
                showToast(this, " "+yxDeviceManager.getDisplayWidth(this));
                break;
            case R.id.getDisplayHeight:
                showToast(this, " "+yxDeviceManager.getDisplayHeight(this));
                break;
            case R.id.switchAutoTime://Permission system
                yxDeviceManager.switchAutoTime(true);
                break;
            case R.id.switchAutoTimeoff://Permission system
                yxDeviceManager.switchAutoTime(false);
                break;
            case R.id.setLanguage://Permission system
                //yxDeviceManager.setLanguage("en", "US");
                yxDeviceManager.setLanguage("zh", "CN");
                break;
            case R.id.setDormantInterval:
                yxDeviceManager.setDormantInterval(this,15000);
                break;
            case R.id.getCPUTemperature:
                showToast(this, " "+yxDeviceManager.getCPUTemperature());
                break;
            case R.id.setNetworkAdb:
                yxDeviceManager.setNetworkAdb(true);
                break;
            case R.id.notsetNetworkAdb:
                yxDeviceManager.setNetworkAdb(false);
                break;
            case R.id.daemon:
                yxDeviceManager.daemon("com.bjw.ComAssistant",0);
                break;
            case R.id.setAppInstallWhitelist:
                yxDeviceManager.setAppInstallWhitelist("true","com.bjw.ComAssistant");
                break;
            case R.id.setAppInstallBlacklist:
                yxDeviceManager.setAppInstallBlacklist("true","com.bjw.ComAssistant");
                break;
            case R.id.removeFileData:
                yxDeviceManager.removeFileData("com.bjw.ComAssistant",false);
                break;
            case R.id.removeFileDatablack:
                yxDeviceManager.removeFileData("com.bjw.ComAssistant",true);
                break;
            case R.id.takeScreenshot:
                yxDeviceManager.takeScreenshot("/sdcard/screenshot.png");//截屏需要一定时间1秒左右,可以使用handle去控制
                break;
            case R.id.updateWifiConfigStore:
                //yxDeviceManager.updateWifiConfigStore("wifi名称","密码");
                yxDeviceManager.updateWifiConfigStore("Sunshine_605","88888888");
                break;
            case R.id.wiegandwrite:
                yxWiegand.write(wiegand);
                break;
            case R.id.wiegandstartreading:
                yxWiegand.startReading(yxWiegandCallBack);
                break;
            case R.id.wiegandstopreading:
                yxWiegand.stopReading();
                break;
            case R.id.wiegandrealese:
                yxWiegand.realese();
                break;
            case R.id.cam_osd_enable:
                yxDeviceManager.setCameraOsdEnable(true);
                break;
            case R.id.cam_osd_disable:
                yxDeviceManager.setCameraOsdEnable(false);
                break;
            case R.id.cam_osd_mirror:
                yxDeviceManager.setCameraOsdMirror(0,true,1);
                break;
            case R.id.cam_osd_pos:
                if(yxDeviceManager.setCameraOsdPosition(2,100,100)){
                    showToast(this,"set ok");
                }
                break;
            case R.id.getTempConf:
                showToast(this,"getTempConf:"+yxDeviceManager.getTempConf());
                break;
            case R.id.setTempConf:
                yxDeviceManager.setTempConf(10);
                break;
            case R.id.getTempOffset:
                showToast(this,"getTempOffset:"+yxDeviceManager.getTempOffset());
                break;
            case R.id.setTempOffset:
                yxDeviceManager.setTempOffset(1);
                break;
            case R.id.getTempStatus:
                showToast(this,"getTempStatus:"+yxDeviceManager.getTempStatus());
                break;
            case R.id.getCompressStatus:
                showToast(this,"getCompressStatus:"+yxDeviceManager.getCompressStatus());
                break;
            case R.id.setCompressStatus:
                yxDeviceManager.setCompressStatus(2);
                break;
            case R.id.getEthernetTetheringAddress:
                showToast(this,"getEthernetTetheringAddress:"+yxDeviceManager.getEthernetTetheringAddress());
                break;
            case R.id.setEthernetTetheringAddress:
                yxDeviceManager.setEthernetTetheringAddress("192.168.1.100/24");
                break;
            case R.id.startEthernetTethering:
                yxDeviceManager.startEthernetTethering();
                break;
            case R.id.stopEthernetTethering:
                yxDeviceManager.stopEthernetTethering();
                break;
            default:
                break;
        }
    }


    public void showToast(Context context, String message){
        Log.d(TAG, message);
        if(toast!=null){
            toast.cancel();
            toast = null;
        }
        if(toast==null){
            toast = Toast.makeText(this, message,Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private String TFPath;
    private String USBPath;
    private BroadcastReceiver mountReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.MEDIA_MOUNTED".equals(intent.getAction())) {
                String path = intent.getData().getPath();
                Log.d(TAG, "externalStoragePath=" + path);
                externalStoragePath = path;
            }
        }
    };

    public Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    yxDeviceManager.setScreenKeyGuard(true);
                    break;
                default:
                    break;
            }
        }
    };
    public static String getTime() {
        SimpleDateFormat sdfTime = new SimpleDateFormat("yyyyMMdd");
        System.out.println("时间戳：" + sdfTime.format(new Date()));
        return sdfTime.format(new Date());
    }
    public static String getRandomNum(Integer length) {
        Random random = new Random();
        String result = "";
        for (int i = 0; i < length; i++) {
            result += random.nextInt(10);
        }
        return result;
    }

}