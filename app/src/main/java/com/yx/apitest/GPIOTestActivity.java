package com.yx.apitest;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.yx.IYxGpioListener;
import android.os.yx.YxDeviceManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class GPIOTestActivity extends AppCompatActivity implements View.OnClickListener{
    private Toast toast;
    private YxDeviceManager yxDeviceManager;
    private boolean bound = false;
    private EditText gpioindex;
    private EditText gpio1index;
    private EditText gpio2index;
    private EditText gpio3index;
    private EditText gpio4index;

    private EditText gpioindexgroup;
    private EditText gpioindexport;
    private EditText gpioindexnumber;
    private TextView gpioindexvalue;
    private String TAG = "yxapitest";
    private IYxGpioListener  aListener= new IYxGpioListener.Stub(){
        @Override
        public void onNewValue(int i) throws RemoteException {
            Log.d(TAG,"1 sec wait,get new value : "+i);
        }
    };
    @Override
    protected void onResume(){
        super.onResume();
        if(bound){
            //registerCallback(null);//if registerCallback gpio is right ,can use it
        }
    }

    private void registerCallback(Object o) {
        if(yxDeviceManager!=null){
            yxDeviceManager.register(aListener,100);
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if(bound){
            //unregisterCallback(null);//if unregisterCallback gpio is right ,can use it
        }
    }

    private void unregisterCallback(Object o) {
        if(yxDeviceManager!=null){
            yxDeviceManager.unregistListener(aListener,100);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpio_test);

        init();
        bound = true;
    }
    public void init(){
        yxDeviceManager = YxDeviceManager.getInstance(this);

        gpioindex = findViewById(R.id.gpioindex);
        gpio1index = findViewById(R.id.gpio1index);
        gpio2index = findViewById(R.id.gpio2index);
        gpio3index = findViewById(R.id.gpio3index);
        gpio4index = findViewById(R.id.gpio4index);

        gpioindexgroup = findViewById(R.id.indexGroup);
        gpioindexport = findViewById(R.id.indexPort);
        gpioindexnumber = findViewById(R.id.indexNumber);
        gpioindexvalue = findViewById(R.id.indexValue);
        InputFilter filtergroup = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                try {
                    int input = Integer.parseInt(dest.toString()+source.toString());
                    if(input>=0&&input<5){
                        return null;
                    }
                }catch (NumberFormatException e){

                }
                return "";
            }
        };
        gpioindexgroup.setFilters(new InputFilter[]{filtergroup});
        InputFilter filterport = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                try {
                    //Log.d(TAG,"!!" + end+"!!"+start +"!!"+dstart +"!!"+dend);
                    int input = end + dend;
                    //Log.d(TAG,"??" + input);
                    if((input>=0&&input<2)){
                        return null;
                    }
                }catch (NumberFormatException e){

                }
                return "";
            }
        };
        gpioindexport.setFilters(new InputFilter[]{filterport});
        InputFilter filternumber = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                try {
                    int input = Integer.parseInt(dest.toString()+source.toString());
                    if(input>=0&&input<8){
                        return null;
                    }
                }catch (NumberFormatException e){

                }
                return "";
            }
        };
        gpioindexnumber.setFilters(new InputFilter[]{filternumber});
        findViewById(R.id.equal).setOnClickListener(this);

        findViewById(R.id.exportGpio_index).setOnClickListener(this);
        findViewById(R.id.exportGpio_current_status).setOnClickListener(this);
        findViewById(R.id.exportGpio_pull_high).setOnClickListener(this);
        findViewById(R.id.exportGpio_pull_low).setOnClickListener(this);

        findViewById(R.id.io1_current_status).setOnClickListener(this);
        findViewById(R.id.io1_pull_high).setOnClickListener(this);
        findViewById(R.id.io1_pull_low).setOnClickListener(this);

        findViewById(R.id.io2_current_status).setOnClickListener(this);
        findViewById(R.id.io2_pull_high).setOnClickListener(this);
        findViewById(R.id.io2_pull_low).setOnClickListener(this);

        findViewById(R.id.io3_current_status).setOnClickListener(this);
        findViewById(R.id.io3_pull_high).setOnClickListener(this);
        findViewById(R.id.io3_pull_low).setOnClickListener(this);

        findViewById(R.id.io4_current_status).setOnClickListener(this);
        findViewById(R.id.io4_pull_high).setOnClickListener(this);
        findViewById(R.id.io4_pull_low).setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.exportGpio_index:
                yxDeviceManager.exportGpio(Integer.parseInt(gpioindex.getText().toString().trim()));
                break;
            case R.id.exportGpio_current_status:
                showToast(this, " " + yxDeviceManager.getGpioDirection(Integer.parseInt(gpioindex.getText().toString().trim())) + yxDeviceManager.getGpioValue(Integer.parseInt(gpioindex.getText().toString().trim())));
                break;
            case R.id.exportGpio_pull_high:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpioindex.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpioindex.getText().toString().trim()),1));
                break;
            case R.id.exportGpio_pull_low:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpioindex.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpioindex.getText().toString().trim()),0));
                break;
            case R.id.io1_current_status:
                showToast(this, " " + yxDeviceManager.getGpioDirection(Integer.parseInt(gpio1index.getText().toString().trim())) + yxDeviceManager.getGpioValue(Integer.parseInt(gpio1index.getText().toString().trim())));
                break;
            case R.id.io1_pull_high:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpio1index.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpio1index.getText().toString().trim()),1));
                break;
            case R.id.io1_pull_low:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpio1index.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpio1index.getText().toString().trim()),0));
                break;
            case R.id.io2_current_status:
                showToast(this, " " + yxDeviceManager.getGpioDirection(Integer.parseInt(gpio2index.getText().toString().trim())) + yxDeviceManager.getGpioValue(Integer.parseInt(gpio2index.getText().toString().trim())));
                break;
            case R.id.io2_pull_high:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpio2index.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpio2index.getText().toString().trim()),1));
                break;
            case R.id.io2_pull_low:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpio2index.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpio2index.getText().toString().trim()),0));
                break;
            case R.id.io3_current_status:
                showToast(this, " " + yxDeviceManager.getGpioDirection(Integer.parseInt(gpio3index.getText().toString().trim())) + yxDeviceManager.getGpioValue(Integer.parseInt(gpio3index.getText().toString().trim())));
                break;
            case R.id.io3_pull_high:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpio3index.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpio3index.getText().toString().trim()),1));
                break;
            case R.id.io3_pull_low:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpio3index.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpio3index.getText().toString().trim()),0));
                break;
            case R.id.io4_current_status:
                showToast(this, " " + yxDeviceManager.getGpioDirection(Integer.parseInt(gpio4index.getText().toString().trim())) + yxDeviceManager.getGpioValue(Integer.parseInt(gpio4index.getText().toString().trim())));
                break;
            case R.id.io4_pull_high:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpio4index.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpio4index.getText().toString().trim()),1));
                break;
            case R.id.io4_pull_low:
                showToast(this, " " + yxDeviceManager.setGpioDirection(Integer.parseInt(gpio4index.getText().toString().trim()),0) + yxDeviceManager.setGpioValue(Integer.parseInt(gpio4index.getText().toString().trim()),0));
                break;
            case R.id.equal:
                //Log.d(TAG,""+(int)gpioindexport.getText().toString().toLowerCase().charAt(0));
                int result = Integer.parseInt(gpioindexgroup.getText().toString().trim())*32+((int) gpioindexport.getText().toString().toLowerCase().charAt(0)-97)*8+Integer.parseInt(gpioindexnumber.getText().toString().trim());
                gpioindexvalue.setText(""+result);
                break;
            default:
                break;
        }
    }

    public void showToast(Context context, String message){
        if(toast!=null){
            toast.cancel();
            toast = null;
        }
        if(toast==null){
            toast = Toast.makeText(this, message,Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
