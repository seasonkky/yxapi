
# API开发文档

文档主要介绍YX API如何快速移植使用， 让开发者能够进一步开发和适配YX硬件平台。



## 如何在Android Studio中使用 API
(1)将yx.jar(或者名为javalib.jar)复制到【工程目录\app\libs\】下；  
![输入图片说明](binary/%E5%9B%BE%E7%89%871.png)  
(2)右键点击libs文件夹中的jar文件选择 add as Library...然后选择Model，这样也可以导入成功  
![输入图片说明](binary/%E5%9B%BE%E7%89%872.png)  
(3)开始使用 API  
注意：所有的api调用前必须声明  
MyManager manager = MyManager.getInstance(this);    
首先要声明 MyManager 对象， 然后就可以开始使用 API.如下面例子：   
//声明 MyManager 对象  
YxDeviceManager manager = YxDeviceManager.getInstance(this);  
//使用 API   
manager.getApiVersion()  
![输入图片说明](binary/%E5%9B%BE%E7%89%873.png)  


## apk安装与卸载

/**
  * @method silentInstallApk(String apkPath) 
  * @description 静默安装apk
  * @param value，apk在文件系统中的绝对路径
  * @return void
*/  
public void silentInstallApk(String apkPath)


/**
  * @method unInstallApk(String packagename) 
  * @description 静默卸载apk
  * @param value，apk包名
  * @return void
*/  
public void unInstallApk(String packagename)


/**
  * @method upgradeSystem(String absolutePath) 
  * @description 升级系统
  * @param value，升级固件的绝对路径
  * @return void
*/  
public void upgradeSystem(String absolutePath)


/**
  * @method setUpdateSystemWithDialog(boolean flag) 
  * @description 设置检测到升级固件时是否显示升级选择对话框
  * @param value，true为显示升级对话框,false为不显示
  * @return void
*/  
public void setUpdateSystemWithDialog(boolean flag)


/**
  * @method setUpdateSystemDelete(boolean flag)
  * @description 设置系统升级后是否删除固件
  * @param value，true为升级后删除固件,false为不删除固件
  * @return void
*/  
public void ssetUpdateSystemDelete(boolean flag)


/**
  * @method silentInstallApk(String apkPath,boolean start)
  * @description 静默安装后选择是否启动
  * @param value，安装包的绝对路径,true为安装后自启动,false则不自启动
  * @return void
*/  
public void silentInstallApk(String apkPath,boolean start)


/**
  * @method silentInstallApk(IYxInstallListener aListener,String apkPath,boolean start)
  * @description 静默安装,带安装状态回调带安装启动
  * @param value，参数aListener为回调服务接口,参数apkPath为安装包的绝对路径,rue为安装后自启动,false则不自启动
  * @return void
*/  
public boolean silentInstallApk(IYxInstallListener aListener,String apkPath,boolean start)

## 网络
/**
  * @method  setDeviceMacaddress(String value)
  * @description 写入设备以太网物理地址，保存到vendor分区，不可擦除
  * @param value，mac address
  * @return -1 写入失败
*/  
public int setDeviceMacaddress(String value)


/**
  * @method  getDeviceMacaddress()
  * @description 获取设备mac地址
  * @param value
  * @return 如果设置有返回设置值,没有则为随机值,默认返回eth0
*/  
public String getDeviceMacaddress()


/**
  * @method  getEthMode()
  * @description 获取以太网模式
  * @param value
  * @return 
*/  
public String getEthMode()


/**
  * @method  getEthStatus()
  * @description 获取以太网状态
  * @param value
  * @return 
*/  
public boolean getEthStatus()


/**
  * @method  getNetMask()
  * @description 获取子网掩码
  * @param value
  * @return 
*/  
public String getNetMask()


/**
  * @method  getGateway()
  * @description 获取网关
  * @param value
  * @return 
*/  
public String getGateway()


/**
  * @method  getEthDns1()
  * @description 获取DNS1
  * @param value
  * @return 
*/  
public String getEthDns1()


/**
  * @method  getEthDns2()
  * @description 获取DNS2
  * @param value
  * @return 
*/  
public String getEthDns2()


/**
  * @method  getIpAddress()
  * @description 获取IP地址
  * @param value
  * @return 
*/  
public String getIpAddress()


/**
  * @method  ethEnabled(boolean enable)
  * @description 以太网开关
  * @param value 参数enable为true打开,false则关闭
  * @return 
*/  
public void ethEnabled(boolean enable)


/**
  * @method  setStaticEthIPAddress(String mIface,String IPaddr, String gateWay,String mask, String dns1,String dns2)
  * @description 设置以太网静态IP地址
  * @param value 参数mIface为网络接口,Ipaddr为IP地址,gateWay为网关,mask为子网掩码,dsn1为域名服务器1,dsn2为域名服务器2
  * @return 
*/  
public void setStaticEthIPAddress(String mIface,String IPaddr, String gateWay,String mask, String dns1,String dns2)


/**
  * @method  setEthernetDhcp(String mIface)
  * @description 设置以太网为DHCP模式
  * @param value 参数mIface为网络接口
*/  
public void setEthernetDhcp(String mIface)


/**
  * @method  updateWifiConfigStore(String SSID,String Password)
  * @description 连接指定WIFI
  * @param value 参数SSID为WIFI名称,Password为密码
*/  
public void updateWifiConfigStore(String SSID,String Password)


/**
  * @method  setWifiStaticIP(String IPaddr, String gateWay,String mask, String dns1,String dns2)
  * @description 设置WIFI静态IP地址，注意前提是当前wifi已连接
  * @param value 参数Ipaddr为IP地址,gateWay为网关,mask为子网掩码,dsn1为域名服务器1,dsn2为域名服务器2
*/  
public void setWifiStaticIP(String IPaddr, String gateWay,String mask, String dns1,String dns2)



## GPIO设置
 gpio-102 (                    |IO4                 ) out hi
 gpio-111 (                    |IO3                 ) out hi
 gpio-112 (                    |IO2                 ) out hi
 gpio-113 (                    |IO1                 ) out hi
/**
  * @method  setGpioDirection(int gpio, int arg)
  * @description 设置GPIO方向
  * @param value，gpio number。1为输入，0为输出
  * @return false 失败 true 成功
*/  
public boolean setGpioDirection(int gpio, int arg)

/**
  * @method  getGpioDirection(int gpio)
  * @description 获取GPIO方向
  * @param value，gpio number
  * @return gpio方向 in / out
*/  
public String getGpioDirection(int gpio)

/**
  * @method  setGpioValue(int id,int value)
  * @description 设置gpio高低，只有在输出模式有效
  * @param value，gpio number，0低1高
  * @return -1失败
*/  
public int setGpioValue(int id,int value)

/**
  * @method  getGpioValue(int id)
  * @description 获取GPIO高低
  * @param value，gpio number
  * @return -1失败 0低 1高
*/  
public int getGpioValue(int id)


/**
  * @method  register(IYxGpioListener aListener,int gpio)
  * @description 注册GPIO电平变化监测回调
  * @param value，参数为回调服务接口, 参数gpio为gpio index
  * @return false 失败 true  :question: 成功
*/  
public boolean register(IYxGpioListener aListener,int gpio)


/**
  * @method  unregistListener(IYxGpioListener aListener,int gpio)
  * @description 取消注册GPIO电平变化监测回调
  * @param value，参数为回调服务接口,参数gpio为gpio index
  * @return void
*/  
public void unregistListener(IYxGpioListener aListener,int gpio) 

## 显示
/**
  * @method  setNavBarNew(boolean flag)
  * @description 设置导航栏显示隐藏
  * @param value，true 显示 false 隐藏
  * @return int 返回值成功为0,错误为-1
*/  
public int setNavBarNew(boolean flag)

/**
  * @method  getNavBarHideState()
  * @description 检测导航栏状态
  * @return 导航栏显示返回true,隐藏返回false
*/  
public boolean getNavBarHideState()

/**
  * @method  setSlideShowNavBar(boolean flag)
  * @description 滑出导航栏功能开启关闭
  * @param value 参数flag为true开启,为false关闭
*/  
public void setSlideShowNavBar(boolean flag)

/**
  * @method  isSlideShowNavBarOpen()
  * @description 检测滑出导航栏功能状态
  * @return 滑出导航栏功能开启返回true,关闭返回false
*/  
public boolean isSlideShowNavBarOpen()

/**
  * @method setSlideShowNotificationBar(boolean flag)
  * @description 滑出通知栏
  * @param value 参数flag为true开启滑出通知栏,为false关闭
*/  
public void setSlideShowNotificationBar(boolean flag)

/**
  * @method isSlideShowNotificationBarOpen()
  * @description 检测滑出通知栏
  * @return 滑出通知栏可用返回true,不可用返回false
*/  
public boolean isSlideShowNotificationBarOpen()


/**
  * @method  setStaBarNew(boolean flag)
  * @description 设置状态栏显示隐藏
  * @param value，true 显示 false 隐藏
  * @return  返回值成功为0,错误为-1
*/  
public int setStaBarNew(boolean flag) 

/**
  * @method  getStatusBar()
  * @description 检测状态栏状态
  * @return  状态栏显示返回true,状态栏隐藏返回false
*/  
public boolean getStatusBar()


/**
  * @method  turnOffHDMI()
  * @description 关闭HDMI输出
  * @param value
  * @return 
*/  
public void turnOffHDMI() 


/**
  * @method  turnOnHDMI()
  * @description 打开HDMI输出
  * @param value
  * @return 
*/  
public void turnOnHDMI()


/**
  * @method  getDpi()
  * @description 获取DPI
  * @return 返回值为系统dpi
*/  
public String getDpi()


/**
  * @method  getDisplayWidth(Context context)
  * @description 获取显示屏分辨率像素宽
  * @param value 参数为当前context
  * @return 返回值为分辨率的宽 
*/  
public int getDisplayWidth(Context context)


/**
  * @method  getScreenHeight(Context context)
  * @description 获取显示屏分辨率像素高
  * @param value 参数为当前context
  * @return 返回值为分辨率的高 
*/  
public int getScreenHeight(Context context)



 /**
  * @method  setScreenRotation(int value)
  * @description 设置系统显示方向,重启后生效
  * @param value 参数设定值为0,90,180,270
  * @return 
*/  
public int setScreenRotation(int value)


 /**
  * @method  getScreenRotation()
  * @description 获取当前系统显示方向
  * @param value 
  * @return 0,90,180,270
*/  
public int getScreenRotation()



 /**
  * @method  turnOffBacklight()
  * @description 关闭背光,测试时注意记住打开背光的方法,防止关闭后屏幕无显示后无法打开背光
  * @param value 
  * @return 
*/  
public void turnOffBacklight()


 /**
  * @method  turnOnBacklight()
  * @description 打开背光
  * @param value 
  * @return 
*/  
public void turnOnBacklight()


 /**
  * @method isBacklightOn()
  * @description 检测背光是否打开
  * @param value 
  * @return 背光打开返回true,关闭返回false
*/  
public boolean isBacklightOn()


 /**
  * @method getSystemBrightness()
  * @description 获取系统背光
  * @param value 
  * @return 返回值为当前系统背光的值
*/  
public int getSystemBrightness()

## 关机和重启

/**
  * @method  shutDownNow()
  * @description 关机
  * @param value，void
  * @return -1失败
*/  
public int shutDownNow()

/**
  * @method  rebootNow()
  * @description 重启
  * @param value，void
  * @return -1失败
*/  
public int rebootNow()


## 系统信息
[参考此demo源码与说明](https://gitee.com/seasonkky/thirdparty/tree/master/DualScreenDemo)

## modem config  
/**
  * @method  getAndroidModle()
  * @description 获取当前设备型号
  * @param value
  * @return -1 失败
*/  
public String getAndroidModle()

/**
  * @method  getTelephonyImei()
  * @description 获取设备IMEI
  * @return 
*/  
public String getTelephonyImei()  

/**
  * @method  getAndroidVersion()
  * @description 获取安卓版本(SDK的API版本号)
  * @param value
  * @return 返回值为当前SDK的API版本号
*/  
public String getAndroidVersion()

/**
  * @method  getRunningMemory()
  * @description 获取内存容量
  * @return 
*/  
public String getRunningMemory()


/**
  * @method  getInternalStorageMemory()
  * @description 获取内部存储容量
  * @return 
*/  
public String getInternalStorageMemory()

/**
  * @method  getKernelVersion()
  * @description 获取内核版本
  * @return 
*/  
public String getKernelVersion()

/**
  * @method  getAndroidDisplay()
  * @description 获取系统版本信息
  * @return 
*/  
public String getAndroidDisplay()

/**
  * @method  getCPUType()
  * @description 获取CPU架构类型
  * @return 
*/  
public String getCPUType()

/**
  * @method  getFirmwareDate()
  * @description 获取固件编译时间
  * @return 
*/  
public String getFirmwareDate()

/**
  * @method  getApiVersion()
  * @description 获取YX_API版本
  * @return 
*/  
public String getApiVersion()

/**
  * @method  getSerialno()
  * @description 获取设备序列号sn
  * @return 
*/  
public String getSerialno()

/**
  * @method  setDeviceSerialno(String value)
  * @description 写入sn号到vendor分区，需要重启才生效
  * @param value 参数为写入sn号的字符串
  * @return 返回值成功为0,错误为-1
*/  
public int setDeviceSerialno(String value)

/**
  * @method  setDeviceCustom(int ID,String value)
  * @description 写入自定义信息到vendor分区
  * @param value 参数ID只允许从16开始,写入自定义信息字符串最大128个字节
  * @return 返回值成功为0,错误为-1
*/  
public int setDeviceCustom(int ID,String value)

/**
  * @method  getDeviceCustom(int ID)
  * @description 获取vendor分区自定义信息
  * @param value 参数ID只允许从16开始
  * @return
*/  
public String getDeviceCustom(int ID)


/**
  * @method  getSimSerialNumber()
  * @description 获取SIM卡number
  * @return sim卡number
*/  
public String getSimSerialNumber()  


## 定时开关机

public void setPowerOnOffWithWeekly(int[] powerOnTime,int[] powerOffTime,int[] weekdays)
/**
* @method setPowerOnOffWithWeekly(int[] powerOnTime,int[] powerOffTime,int[] weekdays)
* @description 周模式设置定时开关机，一天只有一组开关机时间
* @param powerOnTime，开机时间，例如{8,30}。powerOffTime，关机时间，例如{18,30}。
* weekdays，周一到周日工作状态,1 为开机，0 为不开机。例如{1,1,1,1,1,0,0}，是指周一到周五执行开关机
*/
powerOnTime Int[ ] 开机时间，时分 {8,30}
powerOffTime Int[ ] 关机时间，时分 {18,30}
weekdays Int[ ] 周一到周日的工作状态 {1,1,1,1,0,0,1}
注意点：
此方法一天只能有一组时间设入，并且开机时间在前，关机时间在后
范例：
int [] timeonArray = new int{8,30};int [] timeoffArray = new int{18,30};
Int[] weekdays = new int{1,1,1,1,1,0,0};//周一到周日工作状态,1 为开机，0 为不开机
setPowerOnOffWithWeekly(timeonArray,timeoffArray,weekdays);
设置上述时间将会在每周一到周五的 8:30 开机，18:30 关机。


public void setPowerOnOff(int[] powerOnTime,int[] powerOffTime)
/**
* @method setPowerOnOff(int[] powerOnTime,int[] powerOffTime)
* @description 设置一组定时开关机时间数据，需要传入年月日时分
* @param powerOnTime，开机时间，例如{2020,1,10,20,48}，powerOffTime，关机时间，例如{2020,1,10,20,38}。
*/
powerOnTime Int[ ] 开机时间，年月日时分 {2020,01,13,18,40}
powerOffTime Int[ ] 关机时间，年月日时分 {2020,01,13,18,30}
注意点：
此方法设置的时候，关机时间在前，开机时间在后
范例：
int [] timeoffArray = new int{2018,1,10,20,38};
int [] timeonArray = new int{2018,1,10,20,48};
setPowerOnOff(timeonArray,timeoffArray);

设置上述时间将会在 2018 年 1 月 10 号，20:38 关机，20:48 开机
注意：该方法同样适用于只设置开机时间，只需将关机时间传 0 即可，即int [] timeoffArray = new int{0,0,0,0,0};

public String getPowerOnMode()
/**
* @method getPowerOnMode()
* @description 获取定时开关机的模式
* @return "0"是指在定时开关机本地设置的开关机时间。"2"是指用广播的方式调用
setPowerOnOffWithWeekly 方法。"1"是指用广播的方式调用 setPowerOnOff 方法。
*/


public String getPowerOnTime()
/**
* @method getPowerOnTime()
* @description 获取当前设备的开机时间
* @return 返回当前设置的开机时间，例如 202001132025，是指 2020 年 1 月 13 号 20:25开机
*/

public String getPowerOffTime()
/**
* @method getPowerOffTime()
* @description 获取当前设备的关机时间
* @return 返回当前设置的开机时间，例如 202001132020，是指 2020 年 1 月 13 号 20:20关机
*/


public String getLastestPowerOnTime()
/**
* @method getLastestPowerOnTime()
* @description 获取设备上一次执行过的开机时间
* @return 返回设备上一次的开机时间，例如 202001132025，是指在 2020 年 1 月 13 号20:25 执行了开机操作
*/

public String getLastestPowerOffTime()
/**
* @method getLastestPowerOffTime()
* @description 获取设备上一次执行过的关机时间
* @return 返回设备上一次的开机时间，例如 202001132020，是指在 2020 年 1 月 13 号 20:20 执行了开机操作
*/

public void clearPowerOnOffTime()
/**
* @method clearPowerOnOffTime()
* @description 清除定时开关机时间
*/

public boolean isSetPowerOnTime()
/**
* @method isSetPowerOnTime()
* @description 是否设置了定时开关机
* @return 设置了返回true,未设置返回false
*/



## 其他

/**
  * @method  replaceBootanimation(String path)
  * @description 替换开机动画
  * @param value 为要替换的开机动画的绝对路径
*/  
public void replaceBootanimation(String path)  

/**
  * @method  setSystemTime(long modify_time)
  * @description 设置系统时间,当前时间的时间戳
  * @param value 为自1970年1月1日零点起到设定时间的毫秒总数
*/  
public void setSystemTime(long modify_time)  

/**
  * @method  enableWatchdog()
  * @description 使能看门狗
*/  
public void enableWatchdog()

/**
  * @method  disableWatchdog()
  * @description 停止看门狗
*/  
public void disableWatchdog()

/**
  * @method  feedWatchdog()
  * @description 硬件看门狗喂狗最大超时时间为22秒
*/  
public void feedWatchdog()

/**
  * @method  getOtgMode()
  * @description 获取OTG模式
  * @return 返回值host为外接usb模式,返回值peripheral为otg模式
*/  
public String getOtgMode()

/**
  * @method  setOtgMode(boolean value)
  * @description 设置OTG模式
  * @param value 为true打开OTG调试,false为关闭
  * @return 
*/  
public int setOtgMode(boolean value)

/**
  * @method  setScreenKeyGuard(boolean enable)
  * @description 设置屏幕开关
  * @param value 为true打开屏幕,false为关闭屏幕
  * @return 
*/  
public void setScreenKeyGuard(boolean enable)

/**
  * @method  setBugReport(boolean param)
  * @description 设置BUG报告
  * @param value 为true打开BUG报告,false关闭BUG报告
  * @return 
*/  
public int setBugReport(boolean param)

/**
  * @method  getBugReport()
  * @description 获取BUG报告状态
  * @return BUG报告打开返回true,未打开返回false
*/  
public boolean getBugReport()

/**
  * @method  setLedBlink(int mode,int timeout)
  * @description 设置系统状态灯模式
  * @param value mode为设定led工作模式0闪烁,1长亮,2长灭,参数timeout单位为毫秒,作用是调节灯闪烁时间间隔
  * @return 成功返回0,错误返回-1
*/  
public int setLedBlink(int mode,int timeout)

/**
  * @method  setDefaultLauncher(String packageAndClassName)
  * @description 设置默认LAUNCHER
  * @param value 参数为包名加类名,例如"com.android.launcher3/com.android.launcher3.uioverrides.QuickstepLauncher"
*/  
public void setDefaultLauncher(String packageAndClassName)

/**
  * @method  switchAutoTime(boolean flag)
  * @description 开关自动确定网络时间，需要系统权限才可用
  * @param value flag为true开启,为false关闭
*/  
public void switchAutoTime(boolean flag)

/**
  * @method  setLanguage(String language,String country)
  * @description 设置系统语言，需要系统权限才可用
  * @param value language为安卓语言缩写标准,country为安卓国家缩写标准
*/  
public void setLanguage(String language,String country)

/**
  * @method  isAutoSyncTime()
  * @description 获取自动确定网络时间的开关是否开启
  * @param value language为安卓语言缩写标准,country为安卓国家缩写标准
  * @return 返回true为开启,false为未开启
*/  
public boolean isAutoSyncTime()

/**
  * @method  isSetDefaultInputMethodSuccess(String defaultInputMethod)
  * @description 设置默认输出法，需要系统权限才可用
  * @param value defaultInputMethod为需要设置输入法的包名
  * @return 设置成功返回true,失败则为false
*/  
public boolean isSetDefaultInputMethodSuccess(String defaultInputMethod)

/**
  * @method  getDefaultInputMethod()
  * @description 获取当前输入法
  * @return 返回当前输入法包名加类名
*/  
public String getDefaultInputMethod()

/**
  * @method  setDormantInterval(Context context,int time)
  * @description 屏幕休眠时间控制，最大值Integer.MAX_VALUE
  * @param value 参数为当前context,time为时间,取值0和最大值时为永不休眠
*/  
public void setDormantInterval(Context context,int time)

/**
  * @method  getCPUTemperature()
  * @description 获取CPU温度
  * @return 返回值为温度
*/  
public String getCPUTemperature()

/**
  * @method  setNetworkAdb(boolean open)
  * @description 设置网络ADB开关，设置成功后adb服务会重启一下
  * @param value 参数open为true则打开,false则关闭
*/  
public void setNetworkAdb(boolean open)

/**
  * @method  daemon(String packageName, int value)
  * @description 设置守护进程应用
  * @param value 参数为包名,value值为0守护时间30秒,1为60秒,2为180秒,默认30秒
*/  
public void daemon(String packageName, int value)

/**
  * @method  selfStart(String packagname)
  * @description 设置开机自启动APP，APP会优先LAUNCHER启动,启动后可以返回LAUNCHER
  * @param value 参数为包名
*/  
public void selfStart(String packagname)

/**
  * @method  setAppInstallWhitelist(String isopen, String packageName)
  * @description 应用安装白名单
  * @param value 参数isopen为true在应用安装白名单中添加包名packageName并启用,为false则不启用
*/  
public void setAppInstallWhitelist(String isopen, String packageName)

/**
  * @method  setAppInstallBlacklist(String isopen, String packageName)
  * @description 应用安装黑名单
  * @param value 参数isopen为true在应用安装黑名单中添加包名packageName并启用,为false则不启用
*/  
public void setAppInstallBlacklist(String isopen, String packageName)

/**
  * @method  removeFileData(String packagname,boolean flag)
  * @description 
  * @param value 参数packagname为包名,flag为true从黑名单删除,为false从白名单删除
*/  
public void removeFileData(String packagname,boolean flag)

/**
  * @method  getRootStatus()
  * @description 获取系统ROOT用户权限状态
  * @return 返回true为可获取ROOT权限,返回false则不可获取
*/  
public boolean getRootStatus()


/**
  * @method  getSensorReport()
  * @description 获取人体感应sensor当前开关状态
  * @param value
  * @return false 关，true 开
*/  
public boolean getSensorReport() 


/**
  * @method  setSensorReport(boolean param)
  * @description 设置人体感应开关
  * @param false 关， true 开
  * @return >0 成功
*/  
public int setSensorReport(boolean param) 


 /**
  * @method  setCameraOsdEnable(boolean enable)
  * @description 设置摄像头水印开关
  * @param false 关， true 开
*/    
public void setCameraOsdEnable(boolean enable) 


 /**
  * @method  setCameraOsdPosition(int display,int x,int y)
  * @description 设置摄像头水印位置
  * @param display为分辨率，1为1080P(0<x<1568) (0<y<1052), 2为720P(0<x<928) (0<y<692), 3为480P(0<x<288) (o<y<452)
  * @param x y为偶数,且不能同时为0
  * @return false 设置失败，true 设置成功
*/    
public boolean setCameraOsdPosition(int display,int x,int y) 


 /**
  * @method  setCameraOsdMirror(int camid,boolean mirror,int style)
  * @description 设置摄像头水印镜像
  * @param camid为摄像头id， mirror为true打开镜像，为false关闭镜像，style为1为左右镜像，2为上下镜像，3为旋转180度,4为旋转90度，7为旋转270度，8为同时上下和左右镜像
*/    
public void setCameraOsdMirror(int camid,boolean mirror,int style) 


 /**
  * @method  getTempConf()
  * @description 获取目标温度
  * @return -1 获取失败
*/    
public int getTempConf()


 /**
  * @method  setTempConf(int temp)
  * @description 设置目标温度
  * @param 目标温度
*/    
public void setTempConf(int temp)


 /**
  * @method  getTempOffset()
  * @description 获取温度偏移量
  * @return -1 获取失败
*/    
public int getTempOffset()


 /**
  * @method  setTempOffset(int offset)
  * @description 设置温度偏移量
  * @param 温度偏移量
*/    
public void setTempOffset(int offset)


 /**
  * @method  getTempStatus()
  * @description 获取实时温度
  * @return -1 获取失败
*/    
public int getTempStatus()


/**
  * @method  getCompressStatus()
  * @description 获取空压机状态
  * @return -1 获取失败
*/    
public int getCompressStatus()


/**
  * @method  setCompressStatus(int status)
  * @description 设置空压机状态
  * @param 空压机状态
*/    
public void setCompressStatus(int status)